package com.hd.app.photoapp.api.users.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.hd.app.photoapp.api.users.service.shared.UserDTO;

public interface UserService extends UserDetailsService {
	UserDTO createUser(UserDTO userDetails);
	UserDTO getUserDetailsByEmail(String email);
}
