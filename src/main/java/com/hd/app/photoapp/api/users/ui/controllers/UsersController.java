package com.hd.app.photoapp.api.users.ui.controllers;

import java.util.UUID;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hd.app.photoapp.api.users.service.UserService;
import com.hd.app.photoapp.api.users.service.shared.UserDTO;
import com.hd.app.photoapp.api.users.ui.model.CreateUserRequestModel;
import com.hd.app.photoapp.api.users.ui.model.CreateUserResponseModel;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	private Environment env;
	
	@Autowired
	UserService userService;
	
	@GetMapping("/status/check")
	public String status() {
		return "Working on port "+env.getProperty("local.server.port");
	}
	
	@PostMapping(
			consumes = {
			MediaType.APPLICATION_XML_VALUE, 
			MediaType.APPLICATION_JSON_VALUE
			},
		produces = {
			MediaType.APPLICATION_XML_VALUE, 
			MediaType.APPLICATION_JSON_VALUE
				} )
	public ResponseEntity<CreateUserResponseModel> createUser(@Valid @RequestBody CreateUserRequestModel userDetails) {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		
		UserDTO userDto = modelMapper.map(userDetails, UserDTO.class);
		UserDTO createdUser = userService.createUser(userDto);
		
		CreateUserResponseModel returnValue = modelMapper.map(createdUser, CreateUserResponseModel.class);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(returnValue);
	}
	
	
//	@PostMapping(
//			consumes = {
//				MediaType.APPLICATION_XML_VALUE, 
//				MediaType.APPLICATION_JSON_VALUE
//				},
//			produces = {
//				MediaType.APPLICATION_XML_VALUE, 
//				MediaType.APPLICATION_JSON_VALUE
//					} )
//	public ResponseEntity<UserAccount> createUser(@Valid @RequestBody UserAccountRequestModel userAccount) {
//		UserAccount returnValue = new UserAccount();
//		returnValue.setUsername(userAccount.getUsername());
//		returnValue.setPassword(userAccount.getPassword());
//		returnValue.setId(UUID.randomUUID().toString());
//		
//		return new ResponseEntity<UserAccount>(returnValue, HttpStatus.OK);
//	}
//	
//	@DeleteMapping(path="/{id}")
//	public ResponseEntity<Void> deleteUser(@PathVariable String id) {
//		return ResponseEntity.noContent().build();
//	}
}
